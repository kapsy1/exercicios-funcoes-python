""""**Jogo de Craps**
---

Faça um programa de implemente um jogo de Craps. O jogador lança um par de dados, obtendo um valor entre 2 e 12. Se,
na primeira jogada, você tirar 7 ou 11, você um "natural" e ganhou. Se você tirar 2, 3 ou 12 na primeira jogada,
isto é chamado de "craps" e você perdeu devendo R$ 10.000 para a Nat e para o Groger. Se, na primeira jogada, você fez um 4, 5, 6, 8, 9 ou 10,
você perdeu não devendo nada para nós.
"""

from random import randint

def jogoCraps():
    dado1 = randint(1, 6)
    dado2 = randint(1, 6)
    soma_dados = dado1 + dado2
    if soma_dados == 7 or soma_dados == 11:
        return 'Natural, você ganhou!'
    elif soma_dados == 2 or soma_dados == 3 or soma_dados == 12:
        return 'Craps, você perdeu e deve R$ 10.000 para o Groger e para a Nat'
    else:
        return 'Você perdeu! Mas pelo menos não deve na para a Nat e para o Groger'

print(jogoCraps())