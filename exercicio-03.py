"""**Embaralha palavra**
---
Construa uma função que receba uma string como parâmetro e devolva outra string com os carateres embaralhados.
Por exemplo: se função receber a palavra python, pode retornar npthyo, ophtyn ou qualquer outra combinação possível, de forma aleatória.
Padronize em sua função que todos os caracteres serão devolvidos em caixa alta ou caixa baixa, independentemente de como foram digitados.
"""

import random

def embaralhaPalavra(palavra):
    palavra_lista = []
    nova_palavra = ""
    for letra in palavra:
        palavra_lista.append(letra)
    random.shuffle(palavra_lista)
    for item in palavra_lista:
        nova_palavra += item
    return nova_palavra

print(embaralhaPalavra(input('Digite uma palavra para ser embaralhada: ')))
