"""**Data com mês por extenso**
---
Construa uma função que receba uma data no formato DD/MM/AAAA e devolva uma string no formato D de mesPorExtenso de AAAA.
Opcionalmente, valide a data e retorne NULL caso a data seja inválida.
"""

from datetime import datetime

data_string = input('Digite uma data no padrão DD/MM/AAAA ')

def mesPorExtenso():
    try:
        data = datetime.strptime(data_string, '%d/%m/%Y').date()
        dia = data.day
        mes = data.strftime('%B')
        ano = data.year
    except ValueError:
        print('NULL')
    else:
        print(f'Dia {dia} de {mes} de {ano}')

mesPorExtenso()
